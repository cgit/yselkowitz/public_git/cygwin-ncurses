%{?cygwin_package_header}

%global patch 20190727

Name:      cygwin-ncurses
Version:   6.1
Release:   1.%{patch}%{?dist}
Summary:   Ncurses library for Cygwin toolchain

Group:     Development/Libraries
License:   MIT
URL:       http://invisible-island.net/ncurses/
BuildArch: noarch

Source0:   ftp://ftp.invisible-island.net/ncurses/current/ncurses-%{version}-%{patch}.tgz
Patch100:  ncurses-config.patch
Patch101:  ncurses-6.0-abi-version.patch

BuildRequires: gcc

BuildRequires: cygwin32-filesystem
BuildRequires: cygwin32-binutils
BuildRequires: cygwin32-gcc
BuildRequires: cygwin32-gcc-c++
BuildRequires: cygwin32-libtool
BuildRequires: cygwin32

BuildRequires: cygwin64-filesystem
BuildRequires: cygwin64-binutils
BuildRequires: cygwin64-gcc
BuildRequires: cygwin64-gcc-c++
BuildRequires: cygwin64-libtool
BuildRequires: cygwin64


%description
Ncurses library for Cygwin toolchains

%package -n cygwin32-ncurses
Summary:   Ncurses library for Cygwin32 toolchain
Group:     Development/Libraries
Provides:  %{name} = %{version}-%{release}
Obsoletes: %{name} < %{version}-%{release}

%description -n cygwin32-ncurses
Ncurses library for Cygwin i686 toolchain

%package -n cygwin64-ncurses
Summary:   Ncurses library for Cygwin64 toolchain
Group:     Development/Libraries

%description -n cygwin64-ncurses
Ncurses library for Cygwin x86_64 toolchain

%{?cygwin_debug_package}


%prep
%setup -q -n ncurses-%{version}-%{patch}
%patch100 -p1
%patch101 -p2


%build
CYGWIN32_CONFIGURE_ARGS="--with-libtool=%{_bindir}/%{cygwin32_target}-libtool \
	--with-pkg-config-libdir=%{cygwin32_libdir}/pkgconfig"
CYGWIN64_CONFIGURE_ARGS="--with-libtool=%{_bindir}/%{cygwin64_target}-libtool \
	--with-pkg-config-libdir=%{cygwin64_libdir}/pkgconfig"
%cygwin_configure \
	--with-build-cflags=-D_XOPEN_SOURCE_EXTENDED \
	--with-install-prefix=$RPM_BUILD_ROOT \
	--without-debug --disable-relink --disable-rpath \
	--with-ticlib --without-termlib --enable-widec --enable-ext-colors \
	--enable-ext-mouse --enable-sp-funcs --enable-reentrant \
	--with-abi-version=10 --enable-lp64 --with-wrap-prefix=ncwrap_ \
	--enable-sigwinch --enable-colorfgbg --enable-tcap-names \
	--disable-termcap --disable-mixed-case --enable-symlinks \
	--with-pkg-config --enable-pc-files --enable-overwrite \
	--with-manpage-format=normal --with-manpage-aliases \
	--with-default-terminfo-dir=/usr/share/terminfo \
	--enable-echo

sed -i -e 's%^\(LIBRARIES[ \t]*=\).*$%\1 ../lib/libncursesw.la ../lib/libticw.la%' build_*/ncurses/Makefile
%cygwin_make


%install
%cygwin_make install.libs install.includes DESTDIR=$RPM_BUILD_ROOT

for d in %{cygwin32_bindir} %{cygwin64_bindir}
do
	pushd $RPM_BUILD_ROOT$d
	mv ncursesw10-config ncursesw6-config
	ln -s ncursesw6-config ncurses6-config
	sed -i -e 's|echo "10"|echo "6"|' ncursesw6-config
	popd
done

for d in %{cygwin32_includedir}/ncurses{,w} %{cygwin64_includedir}/ncurses{,w}
do
	mkdir -p $RPM_BUILD_ROOT$d
	pushd $RPM_BUILD_ROOT$d
	ln -s ../*.h .
	popd
done

for d in %{cygwin32_libdir} %{cygwin64_libdir}
do
	pushd $RPM_BUILD_ROOT$d
	for f in *.a
	do
		ln -s ${f} ${f/w/}
	done
	popd

	pushd $RPM_BUILD_ROOT$d/pkgconfig
	for f in *.pc
	do
		ln -s ${f} ${f/w/}
	done
	popd
done

# We intentionally don't ship *.la files
find $RPM_BUILD_ROOT -name '*.la' -delete


%files -n cygwin32-ncurses
%doc ANNOUNCE AUTHORS NEWS README
%{cygwin32_bindir}/cygformw-10.dll
%{cygwin32_bindir}/cygmenuw-10.dll
%{cygwin32_bindir}/cygncursesw-10.dll
%{cygwin32_bindir}/cygncurses++w-10.dll
%{cygwin32_bindir}/cygpanelw-10.dll
%{cygwin32_bindir}/cygticw-10.dll
%{cygwin32_bindir}/ncurses6-config
%{cygwin32_bindir}/ncursesw6-config
%{cygwin32_includedir}/*.h
%{cygwin32_includedir}/ncurses/
%{cygwin32_includedir}/ncursesw/
%{cygwin32_libdir}/libform.dll.a
%{cygwin32_libdir}/libformw.dll.a
%{cygwin32_libdir}/libmenu.dll.a
%{cygwin32_libdir}/libmenuw.dll.a
%{cygwin32_libdir}/libncurses.dll.a
%{cygwin32_libdir}/libncursesw.dll.a
%{cygwin32_libdir}/libncurses++.dll.a
%{cygwin32_libdir}/libncurses++w.dll.a
%{cygwin32_libdir}/libpanel.dll.a
%{cygwin32_libdir}/libpanelw.dll.a
%{cygwin32_libdir}/libtic.dll.a
%{cygwin32_libdir}/libticw.dll.a
%{cygwin32_libdir}/pkgconfig/form.pc
%{cygwin32_libdir}/pkgconfig/formw.pc
%{cygwin32_libdir}/pkgconfig/menu.pc
%{cygwin32_libdir}/pkgconfig/menuw.pc
%{cygwin32_libdir}/pkgconfig/ncurses.pc
%{cygwin32_libdir}/pkgconfig/ncursesw.pc
%{cygwin32_libdir}/pkgconfig/ncurses++.pc
%{cygwin32_libdir}/pkgconfig/ncurses++w.pc
%{cygwin32_libdir}/pkgconfig/panel.pc
%{cygwin32_libdir}/pkgconfig/panelw.pc
%{cygwin32_libdir}/pkgconfig/tic.pc
%{cygwin32_libdir}/pkgconfig/ticw.pc

%files -n cygwin64-ncurses
%doc ANNOUNCE AUTHORS NEWS README
%{cygwin64_bindir}/cygformw-10.dll
%{cygwin64_bindir}/cygmenuw-10.dll
%{cygwin64_bindir}/cygncursesw-10.dll
%{cygwin64_bindir}/cygncurses++w-10.dll
%{cygwin64_bindir}/cygpanelw-10.dll
%{cygwin64_bindir}/cygticw-10.dll
%{cygwin64_bindir}/ncurses6-config
%{cygwin64_bindir}/ncursesw6-config
%{cygwin64_includedir}/*.h
%{cygwin64_includedir}/ncurses/
%{cygwin64_includedir}/ncursesw/
%{cygwin64_libdir}/libform.dll.a
%{cygwin64_libdir}/libformw.dll.a
%{cygwin64_libdir}/libmenu.dll.a
%{cygwin64_libdir}/libmenuw.dll.a
%{cygwin64_libdir}/libncurses.dll.a
%{cygwin64_libdir}/libncursesw.dll.a
%{cygwin64_libdir}/libncurses++.dll.a
%{cygwin64_libdir}/libncurses++w.dll.a
%{cygwin64_libdir}/libpanel.dll.a
%{cygwin64_libdir}/libpanelw.dll.a
%{cygwin64_libdir}/libtic.dll.a
%{cygwin64_libdir}/libticw.dll.a
%{cygwin64_libdir}/pkgconfig/form.pc
%{cygwin64_libdir}/pkgconfig/formw.pc
%{cygwin64_libdir}/pkgconfig/menu.pc
%{cygwin64_libdir}/pkgconfig/menuw.pc
%{cygwin64_libdir}/pkgconfig/ncurses.pc
%{cygwin64_libdir}/pkgconfig/ncursesw.pc
%{cygwin64_libdir}/pkgconfig/ncurses++.pc
%{cygwin64_libdir}/pkgconfig/ncurses++w.pc
%{cygwin64_libdir}/pkgconfig/panel.pc
%{cygwin64_libdir}/pkgconfig/panelw.pc
%{cygwin64_libdir}/pkgconfig/tic.pc
%{cygwin64_libdir}/pkgconfig/ticw.pc


%changelog
* Mon Sep 12 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 6.0-7.20160806
- new patch to match distro

* Wed Mar 30 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 6.0-1
- new version

* Mon Jul 01 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 5.9-2.20140524
- Update to latest patch release
- Build for both Cygwin targets

* Mon Jul 01 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 5.7-2
- Rebuild for new Cygwin packaging scheme.
- Build only widechar libraries.

* Thu Mar 22 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 5.7-1
- Initial RPM release.

